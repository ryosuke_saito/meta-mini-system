# Remove unnecessary files

remove_alternatives_opkg () {
	rm -rf ${IMAGE_ROOTFS}/usr/lib/opkg/alternatives
	rmdir ${IMAGE_ROOTFS}/usr/lib/opkg || true
}

ROOTFS_POSTPROCESS_COMMAND += "remove_alternatives_opkg;"

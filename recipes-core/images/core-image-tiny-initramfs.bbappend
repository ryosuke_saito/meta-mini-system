require recipes-core/images/include/cleanup.inc

PACKAGE_INSTALL:remove = "dropbear"

python cttyhack () {
  # Hack for enabling shell job control
  newinit = None
  with open(d.expand('${IMAGE_ROOTFS}/init'), 'r') as init:
    newinit = init.read()
    newinit = newinit.replace('exec sh', 'setsid cttyhack sh && exec sh')
  with open(d.expand('${IMAGE_ROOTFS}/init'), 'w') as init:
    init.write(newinit)
}

IMAGE_PREPROCESS_COMMAND += "cttyhack;"

FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"

# Drop the suid binary
BUSYBOX_SPLIT_SUID = "0"

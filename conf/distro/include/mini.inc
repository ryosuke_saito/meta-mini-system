require conf/distro/poky-tiny.conf

DISTROOVERRIDES .= ":mini"

DISTRO_FEATURES = "pci"

PREFERRED_PROVIDER_virtual/kernel = "linux-yocto-mini"

SERIAL_CONSOLES:qemuall = "115200;ttyAMA0"

VIRTUAL-RUNTIME_dev_manager = "busybox-mdev"
VIRTUAL-RUNTIME_update-alternatives = ""
